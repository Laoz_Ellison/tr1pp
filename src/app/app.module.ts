import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { AddVComponent } from "./add-v/add-v.component";
import { LoginComponent } from "./login/login.component";
import { FeaturesComponent } from "./features/features.component";
import { PricingComponent } from "./pricing/pricing.component";
import { LogoutComponent } from "./logout/logout.component";
import { PaymentComponent } from "./payment/payment.component";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { ProfileComponent } from "./profile/profile.component";

const appRoutes: Routes = [
  { path: "", component: LoginComponent },
  { path: "home", component: HomeComponent },
  { path: "features", component: FeaturesComponent },
  { path: "pricing", component: PricingComponent },
  { path: "logout", component: LoginComponent },
  { path: "profile", component: ProfileComponent },
  { path: "home/:id/:name/:price/:imagepath", component: PaymentComponent },
  { path: "addv", component: AddVComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddVComponent,
    LoginComponent,
    FeaturesComponent,
    PricingComponent,
    LogoutComponent,
    PaymentComponent,
    HeaderComponent,
    FooterComponent,
    ProfileComponent
  ],
  imports: [BrowserModule, RouterModule.forRoot(appRoutes), FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
