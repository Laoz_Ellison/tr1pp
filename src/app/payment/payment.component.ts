import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router, Params } from "@angular/router";

@Component({
  selector: "app-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.css"]
})
export class PaymentComponent implements OnInit {
  constructor(public route: ActivatedRoute, public router: Router) {}
  payment: { id: number; name: string; price: number; imagepath: string };

  ngOnInit() {
    this.payment = {
      id: this.route.snapshot.params["id"],
      name: this.route.snapshot.params["name"],
      price: this.route.snapshot.params["price"],
      imagepath: this.route.snapshot.params["imagepath"]
    };
    this.route.params.subscribe((params: Params) => {
      this.payment.id = params["id"];
      this.payment.name = params["name"];
      this.payment.price = params["price"];
      this.payment.imagepath = params["imagepath"].replace(/__/g, "/");
    });

    console.log(
      this.payment.id,
      this.payment.name,
      this.payment.price,
      this.payment.imagepath
    );
  }

  navigate() {
    this.router.navigate(["pricing"]);
  }
}
