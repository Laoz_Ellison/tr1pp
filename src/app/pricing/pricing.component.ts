import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-pricing",
  templateUrl: "./pricing.component.html",
  styleUrls: ["./pricing.component.css"]
})
export class PricingComponent implements OnInit {
  constructor() {}
  otp = true;
  message = true;
  button = false;
  cardshit = false;

  ngOnInit() {}

  submit() {
    this.otp = false;
    this.message = false;
    this.button = true;
    this.cardshit = true;
  }
  pay() {}
}
