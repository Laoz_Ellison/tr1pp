import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  constructor(public route: ActivatedRoute, public router: Router) {}

  Vouchers = [
    {
      id: 1,
      name: "Konga",
      price: 35000,
      imagepath: "..__..__assets__public__image__bg.jpg"
    },
    {
      id: 2,
      name: "Amazon",
      price: 165000,
      imagepath: "..__..__assets__public__image__bg.jpg"
    },
    {
      id: 3,
      name: "Jumia",
      price: 265000,
      imagepath: "..__..__assets__public__image__bg.jpg"
    },
    {
      id: 4,
      name: "Ali Xpress",
      price: 285000,
      imagepath: "..__..__assets__public__image__bg.jpg"
    },
    {
      id: 5,
      name: "Taxify",
      price: 5000,
      imagepath: "..__..__assets__public__image__bg.jpg"
    },
    {
      id: 6,
      name: "Uber",
      price: 2000,
      imagepath: "..__..__assets__public__image__bg.jpg"
    },
    {
      id: 7,
      name: "Wallmart",
      price: 10000,
      imagepath: "..__..__assets__public__image__bg.jpg"
    },
    {
      id: 8,
      name: "ralph",
      price: 225000,
      imagepath: "..__..__assets__public__image__bg.jpg"
    }
  ];

  navigate(id, name, price, imagepath) {
    // import { Router, ActivatedRoute } from "@angular/router";
    const regex = "/";
    this.router.navigateByUrl(`/home/${id}/${name}/${price}/${imagepath}`);
  }

  ngOnInit() {}
}
